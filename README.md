# Clustering Based on Neighbour Distance

This is a clustering algorithm which will cluster an unknown set of points based on the distance between each point and its neighbours.<br>

## Given Inputs

1) A list of unknown points.
2) A list of points which are the heads of each cluster.

## Algorithm

1) Obtain the distance of every known point with every other point.
2) Sort the distances computed in ascending order.
3) For every value of computed distance in the sorted order:
    1) Obtain the pair of points which are seperated by this distance.
    2) If neither of these points are known to belong to any particular cluster:<br>
    Then tag both of them as belonging to the same cluster.
    3) If exactly one of them is known to belong to a particular cluster:<br>
    Then tag the other point and and all points associated to it as being part of this particular cluster.
    4) If both of them are known to belong to some particular clusters:<br>
    Do nothing.

In the end you will have all the unknown points tagged as being part of exactly one cluster.

## Usage

The file Cluster.py in this repository contains classes which help in clustering of points by this algorithm.

You may import this file and use these classes as needed.

The Cluster.py file also contains an example on how these classes are to be used, which can be executed by running the following command on you terminal:

```
python Cluster.py
```

### Class Descriptions :

#### Class Object

This class contains the data about each point. This includes:
1) Attribute Point:<br>
A tuple storing the coordinates of the point.
2) Attribute ID:<br>
An int storing the ID of the point set by the Collection class.
3) Attribute IsHead:<br>
A bool representing whether or not the point is the head of a cluster.
4) Attribute Group:<br>
A set of ID's of points which belons to the same cluster as the point represented in this class.

#### Class Collection

This class is used to store the data about which all points are present in the system. This class is also respoisible for performing the clustering operation as well.

This class has the following Attributes:
1) Attribute NewID:<br>
An int representing the ID which will be assigned to a point newly added to the system the Collection class represents.
2) Attribute Objs:<br>
A dict mapping the ID's of a point to their objects of class Object.
3) Attribute Heads:<br>
A set containing the ID's of the points which Heads of clusters. Each cluster may only have one head. So each Head is taken to be the head of a different cluster.

This class has the following Methods:
1) AddObj(self, Point, IsHead=False)<br>
    * Parameters:
        1) Point:<br>
        A tuple representing the coordinates of the point being added to the system.
        2) IsHead: (Optional)<br>
        A bool representing whether or not the point being added is the head of a cluster.     
    * Function:<br>
    Adds the point to the system represented by the Collection class. This is done by creating an object of class Object with the given coordinate and the state of being Head.   
2) FindHead(self, id)<br>
    * Parameters:
        1) id:<br>
        An int representing the ID of the point about which the query is done.
    * Function:<br>
    Scans through the Attribute Group of the objects of class Object of points which are heads of clusters for the ID of the point of interest, id.
    * Returns:
        1) A list of int:<br>
        This list represents the ID's of points which are heads of clusters and contain the given id in their Group.
3) Group(self, id1, id2)
    * Parameters:
        1) id1:<br>
        In int representing the ID of one of the points on which grouping will be attempted.
        2) id2:<br>
        In int representing the ID of the other point on which grouping will be attempted.
    * Function:<br>
    Attempts to group the two points based on the logic explained in Step 3 of Algorithm section.
    * Returns:
        1) A bool:<br>
        This bool represents whether or not the two points were tagged as being in the same cluster.
4) Cluster(self, SubPlot=None):
    * Parameters:
        1) SubPlot: (Optional)<br>
        A subplot of a pyplot figure. If provided, this method will draw black lines as to show how the chains of the cluster formed.
    * Function:<br>
    Performs the clustering operation as explained in the Algorithm section.

## Required Dependencies

The following libraries must be installed before using the code contained in the Cluster.py file:

* MatPlotLib
* NumPy
* SciPy
* Random

If you have pip installed, then you may easily install these dependencies by running the following command on your terminal:
```
pip install -r requirements.txt
```