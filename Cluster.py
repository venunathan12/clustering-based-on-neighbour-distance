from scipy.spatial.distance import cdist
from matplotlib import pyplot
import random
import numpy

class Object:
    def __init__(self, Point, ID, IsHead=False):
        self.Point = Point
        self.IsHead = IsHead
        self.Group = {ID}
        self.ID = ID

class Collection:
    def __init__(self):
        self.NewID = 0
        self.Objs = {}
        self.Heads = set({})
    
    def AddObj(self, Point, IsHead=False):
        self.Objs[self.NewID] = Object(Point, self.NewID, IsHead)        
        if IsHead:
            self.Heads.add(self.NewID)
        self.NewID += 1
    
    def FindHead(self, id):
        Hd = []
        for H in self.Heads:
            if id in self.Objs[H].Group:
                Hd.append(H)
        return Hd
    
    def Group(self, id1, id2):
        H1s = self.FindHead(id1); H2s = self.FindHead(id2)
        if len(H1s):
            if len(H2s):
                return False
            
            for ns in self.Objs[id2].Group:
                self.Objs[H1s[0]].Group.add(ns)
            return True
        
        if len(H2s):
            for ns in self.Objs[id1].Group:
                self.Objs[H2s[0]].Group.add(ns)
            return True
        
        G1 = self.Objs[id1].Group.copy()
        G2 = self.Objs[id2].Group.copy()
        if G1 == G2:
            return False

        for g1 in G1:
            for g2 in G2:
                self.Objs[g1].Group.add(g2)
                self.Objs[g2].Group.add(g1)
        return True
    
    def Cluster(self, SubPlot=None):
        Pts = [O.Point for O in self.Objs.values()]
        D = cdist(Pts, Pts); M = numpy.max(D)
        for x in range(len(Pts)):
            y = x
            while y < len(Pts):
                D[x,y] = M+1
                y += 1

        Flat = D.reshape((-1,))
        Ars = Flat.argsort()
        for A in Ars:
            if Flat[A] == M+1:
                break        
            row = A//len(Pts)
            col = A%len(Pts)

            if self.Group(row, col) and SubPlot != None:
                SubPlot.plot([self.Objs[row].Point[0],self.Objs[col].Point[0]], [self.Objs[row].Point[1],self.Objs[col].Point[1]], c=(0,0,0))                

def GenRandomRadial(Cent, Radius):
    R = random.uniform(0,Radius)
    Th = random.uniform(0, 2*numpy.pi)
    X = Cent[0] + R*numpy.cos(Th)
    Y = Cent[1] + R*numpy.sin(Th)
    return (X, Y)

if __name__ == "__main__":
    Fig = pyplot.figure(figsize=(8,8))
    P = Fig.subplots(nrows=1, ncols=1)

    C = Collection()
    C.AddObj((-1,-1), True); C.AddObj((1,1), True); C.AddObj((1,-1), True); C.AddObj((-1,1), True)
    for i in range(250):
        C.AddObj(GenRandomRadial((1,1), 1))
        C.AddObj(GenRandomRadial((-1,-1), 1))
        C.AddObj(GenRandomRadial((1,-1), 1))
        C.AddObj(GenRandomRadial((-1,1), 1))

    C.Cluster()

    for (id, O) in C.Objs.items():
        if len(C.FindHead(id)) is 1:
            if C.FindHead(id)[0] == 0:
                Cl = (1,0,0)
            elif C.FindHead(id)[0] == 1:
                Cl = (0,0,1)
            elif C.FindHead(id)[0] == 2:
                Cl = (0,1,0)
            elif C.FindHead(id)[0] == 3:
                Cl = (1,1,0)
        else:
            Cl = (0,0,0)
        P.plot(O.Point[0], O.Point[1], 'o', c=Cl)
    pyplot.show()
    